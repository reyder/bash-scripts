#!/bin/bash
# Woox.pl © 2016
# backup script

# Changelog 14.04.2016
# -- hide mysql backup data (not using SSL for connection, no need)
# -- remove TS backup LOL

# Notes 19.12.2015
# .xz compression uses only one core (mulicore support comming *SOON TM*, [don't depends on me])
# UPDATE multicore support is here ! but not in default version aka jessie ;-( gash..
# WARNING output from --reload (depends on files) TO DO


TIMESTAMP=$(date +"%F")
BACKUP_DIR="/home/reyder/backup/$TIMESTAMP"

# Rar options
# -ep1 --> remove full path from dirs
# -ma5 --> using rar5 format
# -m5  --> use the highest level of compression
# -hp   --> protect archive with pasword where even file lists are encrypted :0
RAR_PASSWORD="backup_for_the_glory_w00x"
RAR_VARS="-hp$RAR_PASSWORD -ep1 -ma5 -m5"

# Account name for backup
ACC_BACKUP_NAME="reyder"

# Mysql data
MYSQL=/usr/bin/mysql
MYSQLDUMP=/usr/bin/mysqldump
MYSQL_EXT_FILE="/home/reyder/.mysqldump"

# Mega data
MEGA_BACKUP_DIRECTORY="/Root/Backup/woox/"

# Get all dirs ready to backup
DIRS_TO_BACKUP=`du --max-depth=0 --threshold=-2GB /home/*/www/* | cut -f2`
ALL_DIRS=$(printf ",%s" "${DIRS_TO_BACKUP[@]}")
ALL_DIRS=${ALL_DIRS:1}

# Creating directory for a fresh backup
mkdir -p $BACKUP_DIR/{mysql,html,ts}

# Get databases to backup
databases=`$MYSQL --defaults-extra-file=$MYSQL_EXT_FILE --skip-ssl -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema|performance_schema|phpmyadmin)"`

# dump and compress databeses
for db in $databases; do
	$MYSQLDUMP --defaults-extra-file=$MYSQL_EXT_FILE --force --routines --triggers --opt --databases $db | xz -9e > "$BACKUP_DIR/mysql/$db.xz"
done

# Backup all HTMl directories
rar a $RAR_VARS $BACKUP_DIR/html/html-$TIMESTAMP.rar $ALL_DIRS

# Set proper owner
chown -R $ACC_BACKUP_NAME:$ACC_BACKUP_NAME $BACKUP_DIR

# Setting privileges to protect files
chmod -R 700 $BACKUP_DIR

# Backup size
BACKUP_SIZE=$(du --max-depth=0 --bytes $BACKUP_DIR | awk '{ print $1 }')

# Check free storage (Mega)
MEGA_FREE_SPACE=$(su $ACC_BACKUP_NAME -c "megadf --reload --free")

if [ $BACKUP_SIZE > $MEGA_FREE_SPACE ] ; then
	# Delete old backups from local drive
	find $(dirname $BACKUP_DIR) -type d -mtime +8 -print0 | xargs -0 rm -r --

	# Delete first global path, and 3 last lines (we gonna keep them)
	FILES_TO_DELETE=$(su $ACC_BACKUP_NAME -c "megals $MEGA_BACKUP_DIRECTORY | tail -n +2 | head -n -3")

	su $ACC_BACKUP_NAME -c "megarm --reload $FILES_TO_DELETE"
fi

su $ACC_BACKUP_NAME -c "megamkdir --reload $MEGA_BACKUP_DIRECTORY$TIMESTAMP"
su $ACC_BACKUP_NAME -c "megacopy --reload -l $BACKUP_DIR -r $MEGA_BACKUP_DIRECTORY$TIMESTAMP"
