#!/bin/bash
#
# made in inetive.pl
# update by spacedust
#


SMARTCTL=`which smartctl 2>/dev/null`

if [ -z $SMARTCTL ] || [ ! -x $SMARTCTL ]; then
    echo "It seems you have no smartmontools installed. Install it to proceed."
    exit 1
fi

function check {
    tmp=`mktemp`
    $SMARTCTL -A $1 | sed -r 's/^\s+//g' > $tmp
    for val in 5 10 11 187 188 196 197 198 199 200; do

	print=`egrep "^$val " $tmp | awk '{ print $2" = "$10 }'`
	if [ ! -z "$print" ]; then
		echo -n "$1: "
	    if [ `echo $print | awk '{ print $3 }'` != 0 ]; then
		echo -e "$val : \033[31;2m$print\033[0m"
	    else
		echo -e "$val : \033[32;2m$print\033[0m"
	    fi
	fi
    done
    echo
    rm -f $tmp
}

if [ $# -gt 0 ]; then
    devices=$@
else
    devices=`ls /dev/sd[a-z]`
fi
echo
if [ -b /dev/twe0 ]; then
    check "-d 3ware,0 /dev/twe0"
    check "-d 3ware,1 /dev/twe0"
else
    for d in $devices; do
	check $d
    done
fi
