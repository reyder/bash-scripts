#!/bin/bash
#
# Control script
# Version 2.3
#
# Copyright (C) 2016  Woox.pl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

######################## Variables ########################
ACTION=$1
USER=$2
PATH_SCRIPT="$(readlink -f ${BASH_SOURCE[0]})"
DIR_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
RUTORRENT_PATH="/var/rutorrent"
ARCHIVE_PATH="/root/archive"

RED="\e[1;97;41m"
GREEN="\e[1;97;42m"
YELLOW="\e[1;97;43m"
END="\e[0m"
###########################################################

checkUser() {
	if [ -z "$(getent passwd $USER)" ] ; then
		echo "$USER - nie ma takiego konta"
		exit
	elif [ `passwd -S $USER |awk '{print $2}'` = 'L' ] ; then
		ACC_STATUS='L'
	else
		ACC_STATUS=''
	fi

	USER_PATH=`sh -c "echo ~$USER"`

	HAVE_RT=`ls $RUTORRENT_PATH/conf/users | grep -w $USER | wc -l`

	if [ $HAVE_RT != '0' ] ; then
		IS_RT_STARTED=`pgrep -lfu $USER rtorrent > /dev/null && echo 1 || echo 0`
	fi

	if [ -d "$USER_PATH/.vnc" ]; then
		HAVE_VNC='1'
		IS_VNC_STARTED=`pgrep -u $USER vnc > /dev/null && echo 1 || echo 0`
		if [ $IS_VNC_STARTED != '0' ] ; then
			VNC_PORT=`ps aux | grep $USER | egrep -o "Xvnc-core[4]{0,1} \:[0-9]{1,2}" | awk 'BEGIN { FS=":" } { print $2 }'`
		fi
	else
		HAVE_VNC='0'
	fi

	HAVE_IRSSI=`ls -al $USER_PATH | grep -w .irssi | wc -l`

	if [ $HAVE_IRSSI != '0' ] ; then
		IS_IRSSI_STARTED=`pgrep -lfu $USER irssi > /dev/null && echo 1 || echo 0`
	fi
}

prepare_create() {
	echo "Wprowadz nick: "
	read USER
	echo "Wprowadz haslo: "
	read PASSWORD
	echo "Wprowadz pojemnosc dysku (GB) [0 - bez limitu]: "
	read DISK
	echo "Czy zainstalowac rtorrenta ? (y/n)"
	read RTORRENT

	if [ "$RTORRENT" = "y" ] ; then
		echo "Podaj liczbe aktywnych procesow: [0 - bez limitu]"
		read RTORRENTPROCESS
	fi

	echo "Czy zainstalowac irssi ? (y/n)"
	read IRSSI

	echo "Czy zainstalowac VNC? (y/n)"
	read VNC


	# Let's begin..
	if [ "$USER" != "" ] && [ "$PASSWORD" != "" ] && [ "$DISK" != "" ] ; then
		create_account
	fi

	if [ "$RTORRENT" = "y" ] && [ "$RTORRENTPROCESS" != "" ] && [ "$USER" != "" ] && [ "$PASSWORD" != "" ] ; then
		create_rtorrent
	fi

	if [ "$IRSSI" = "y" ] && [ "$USER" != "" ] && [ "$RTORRENT" = "y" ] ; then
		create_irssi
	fi

	if [ "$USER" != "" ] && [ "$DISK" != "" ] && [ "$DISK" != 0 ] ; then
		set_quota
	fi

	if [ "$USER" != "" ] && [ "$VNC" = "y" ]  ; then
		create_vnc
	fi
}

create_account() {
	(echo $PASSWORD; echo $PASSWORD; echo "" ; echo "" ; echo "" ; echo "" ; echo "" ;  echo "Y") | adduser $USER >/dev/null 2>&1
	USER_PATH=`sh -c "echo ~$USER"`

	echo "Login : $USER"
	echo "Hasło : $PASSWORD"
	echo "Katalog domowy : $USER_PATH"
}

create_rtorrent() {
	# Create "random" unique port for rtorrent
	PORT=`id $USER | awk -F" " '{ print $1 }' | grep -oE "[0-9]{4}" | head -1`
	PORT=`expr 22000 + $PORT`

	# Create config directories for rutorrent
	mkdir -p $RUTORRENT_PATH/conf/users/$USER/plugins
	echo -e "<?php\n\$scgi_port = 0;\n\$scgi_host = \"unix://$USER_PATH/.rtorrent/session/rpc.socket\";\n?>" > $RUTORRENT_PATH/conf/users/$USER/config.php

	# Create rest of files for rutorrent
	mkdir $RUTORRENT_PATH/share/users/$USER
	mkdir $RUTORRENT_PATH/share/users/$USER/{torrents,settings}

	# Create directories for rtorrent
	mkdir -p $USER_PATH/{downloads,\.rtorrent,\.rtorrent/session,\.rtorrent/watch}

	# Create rtorrent config file
	if [ "$RTORRENTPROCESS" != 0 ] ; then
		ECHORTPROCESS="scheduler.max_active.set = $RTORRENTPROCESS\n"
	else
		ECHORTPROCESS=""
	fi

	echo -e "execute = {sh,-c,rm -f $USER_PATH/.rtorrent/session/rpc.socket}\nscgi_local = $USER_PATH/.rtorrent/session/rpc.socket\nexecute = {sh,-c,chmod 0666 $USER_PATH/.rtorrent/session/rpc.socket}\nencoding_list = UTF-8\nsystem.umask.set = 022\nport_range = $PORT-$PORT\nport_random = no\ncheck_hash = no\ndirectory = $USER_PATH/downloads\nsession = $USER_PATH/.rtorrent/session\nschedule = watch_directory,1,1,\"load_start=$USER_PATH/.rtorrent/watch/*.torrent\"\ndht = disable\npeer_exchange = no\nnetwork.http.ssl_verify_peer.set=0\n${ECHORTPROCESS}execute = {sh,-c,/usr/bin/php $RUTORRENT_PATH/php/initplugins.php $USER &}" > $USER_PATH/.rtorrent.rc

	# Create password for web-browser
	PASSWD=`perl -le "print crypt(\"$PASSWORD\",\"$(cat /dev/urandom | tr -dc 0-9 | head -c4)\")"`
	echo "$USER:$PASSWD" >> /usr/local/nginx/rutorrent_passwd

	# Setting privilages
	chown -R nginx:$USER $RUTORRENT_PATH/conf/users/$USER
	chmod -R 750 $RUTORRENT_PATH/conf/users/$USER

	chown -R $USER:$USER $USER_PATH/{downloads,\.rtorrent}

	chown root:root $USER_PATH/.rtorrent.rc
	chmod 644 $USER_PATH/.rtorrent.rc

	chown -R nginx:$USER $RUTORRENT_PATH/share/users/$USER
	chmod -R 750 $RUTORRENT_PATH/share/users/$USER

	echo "Panel ruTorrent	: http://$HOSTNAME"
}

create_irssi() {
	# Create random stuff for irssi
	PORT_WEBUI=`shuf -i 2000-65000 -n 1`
	PASSWORD_WEBUI=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 7 | head -n 1)

	mkdir -p $USER_PATH/.irssi/scripts/autorun
	wget --directory-prefix=$USER_PATH/.irssi/scripts https://github.com/autodl-community/autodl-irssi/archive/master.zip >/dev/null 2>&1
	unzip -o $USER_PATH/.irssi/scripts/master.zip -d $USER_PATH/.irssi/scripts/ >/dev/null 2>&1
	mv $USER_PATH/.irssi/scripts/autodl-irssi-master/* $USER_PATH/.irssi/scripts/
	rm -R $USER_PATH/.irssi/scripts/autodl-irssi-master $USER_PATH/.irssi/scripts/master.zip
	mv $USER_PATH/.irssi/scripts/autodl-irssi.pl $USER_PATH/.irssi/scripts/autorun/
	mkdir -p $USER_PATH/.autodl
	touch $USER_PATH/.autodl/autodl.cfg

	echo -e "[options]\ngui-server-port = $PORT_WEBUI\ngui-server-password = $PASSWORD_WEBUI\n" > $USER_PATH/.autodl/autodl.cfg

	mkdir -p $RUTORRENT_PATH/conf/users/$USER/plugins/autodl-irssi

	echo -e "<?php\n\$autodlPort = $PORT_WEBUI;\n\$autodlPassword = \"$PASSWORD_WEBUI\";\n?>" > $RUTORRENT_PATH/conf/users/$USER/plugins/autodl-irssi/conf.php

	# Setting privilages
	chown -R $USER:nginx $USER_PATH/.irssi
	chown -R $USER:nginx $USER_PATH/.irssi/scripts
	chmod -R 771 $USER_PATH/.irssi/scripts
	chown -R $USER:$USER $USER_PATH/.autodl

	chown -R nginx:$USER $RUTORRENT_PATH/conf/users/$USER/plugins/autodl-irssi
	chmod -R 750 $RUTORRENT_PATH/conf/users/$USER/plugins/autodl-irssi
}

create_vnc() {
	su $USER -c "vncserver"
	VNC_PORT=`ps aux | grep $USER | egrep -o "Xvnc-core[4]{0,1} \:[0-9]{1,2}" | awk 'BEGIN { FS=":" } { print $2 }'`

	echo "Adres VNC: $HOSTNAME:$VNC_PORT"
}

set_quota() {
	if hash quotatool 2>/dev/null; then
		DISK_BYTES=`expr 1024 \* 1024 \* $DISK`
		MOUNT_POINT=`df -h -k $USER_PATH |awk '{print $1}' | grep /`

		quotatool -u $USER -bq $DISK_BYTES -l $DISK_BYTES $MOUNT_POINT
	else
		echo "error: quotatool is not installed"
	fi
}

start() {
	checkUser

	if [ $HAVE_RT != '0' ] && [ $IS_RT_STARTED != '0' ] ; then
		echo "$USER - rtorrent jest już uruchomiony"
	elif [ $HAVE_RT != '0' ] && [ $IS_RT_STARTED = '0' ] ; then
		su -s /bin/bash -c "screen -dmS rtorrent -t rtorrent rtorrent" $USER
		echo "$USER - czekam 5 sekund na potwierdzenie działania rtorrenta"
		sleep 5s
		checkUser
		if [ $IS_RT_STARTED != '0' ] ; then
			echo "$USER - rtorrent został prawidłowo uruchomiony"
		else
			echo "$USER - kasuje pliki rtorrenta i sesji screen"
			rm -f $USER_PATH/.rtorrent/session/rpc.socket
			rm -f $USER_PATH/.rtorrent/session/rtorrent.lock
			rm -fr /var/run/screen/S-$USER
			su -s /bin/bash -c "screen -dmS rtorrent -t rtorrent rtorrent" $USER
			echo "$USER - czekam 5 sekund na potwierdzenie działania rtorrenta"
			sleep 5s
			checkUser
			if [ $IS_RT_STARTED != '0' ] ; then
				echo "$USER - rtorrent został prawidłowo uruchomiony"
			else
				echo "$USER - rtorrent wciąż nie działa muszę się przyjrzeć temu problemowi"
			fi
		fi
	fi

	if [ $HAVE_IRSSI != '0' ] && [ $IS_IRSSI_STARTED = '0' ] ; then
		su -s /bin/bash -c "screen -dmS irssi irssi" $USER
		echo "$USER - czekam 5 sekund na potwierdzenie działania irssi"
		sleep 5s
		checkUser
		if [ $IS_IRSSI_STARTED != '0' ] ; then
			echo "$USER - irssi zostało prawidłowo uruchomione"
		else
			echo "$USER - irssi NIE zostało prawidłowo uruchomione"
		fi
	fi

	if [ $HAVE_VNC != '0' ] ; then
		if [ $IS_VNC_STARTED != '0' ] ; then
			echo "$USER - VNC jest już uruchomione"
		elif [ $IS_VNC_STARTED = '0' ] ; then
			VNC_PORT=`ls $USER_PATH/.vnc | egrep -o "\:[0-9]{1,2}" | head -n1 | awk 'BEGIN { FS=":" } { print $2 }'`
			rm -R "/tmp/.X$VNC_PORT-lock" > /dev/null 2>&1
			rm -R  "/tmp/.X11-unix/X$VNC_PORT" > /dev/null 2>&1
			su -s /bin/bash -c "cd $USER_PATH ; vncserver :$VNC_PORT -geometry 1920x1080 -randr 1920x1080,1600x1200,1440x900,1024x768" $USER  > /dev/null 2>&1
			checkUser
			if [ $IS_VNC_STARTED != '0' ] ; then
				echo "$USER - VNC zostało prawidłowo uruchomione"
			else
				echo "$USER - VNC wciąż nie działa muszę się przyjrzeć temu problemowi"
			fi
		fi
	fi
}

stop() {
	checkUser

	if [ $HAVE_RT != '0' ] ; then
		if [ $IS_RT_STARTED != '0' ] ; then
			su -s /bin/bash -c "screen -S rtorrent -X quit ; screen -wipe" $USER > /dev/null 2>&1
			echo "$USER - rtorrent został zatrzymany"
		else
			echo "$USER - rtorrent jest już zatrzymany"
		fi
	fi

	if [ $HAVE_VNC != '0' ] ; then
		if [ $IS_VNC_STARTED != '0' ] ; then
			su -s /bin/bash -c "vncserver -kill :$VNC_PORT 2> /dev/null" $USER > /dev/null 2>&1
			echo "$USER - VNC zostało zatrzymane"
		else
			echo "$USER - VNC jest już zatrzymane"
		fi
	fi

	if [ $HAVE_IRSSI != '0' ] ; then
		if [ $IS_IRSSI_STARTED != '0' ] ; then
			su -s /bin/bash -c "screen -S irssi -X quit ; screen -wipe" $USER > /dev/null 2>&1
			echo "$USER - IRRSSI zostało zatrzymane"
		else
			echo "$USER - IRSSI jest już zatrzymane"
		fi
	fi
}


restart() {
	checkUser
	stop
	start
}

status() {
	checkUser

	VNC_COLOR=`[[ $HAVE_VNC = '0' ]] && echo "$RED" || ([[ $IS_VNC_STARTED = '1' ]] && echo "$GREEN" || echo "$YELLOW")`
	RT_COLOR=`[[ $HAVE_RT = '0' ]] && echo "$RED"  || ([[ $IS_RT_STARTED = '1' ]] && echo "$GREEN" || echo "$YELLOW")`
	IRSSI_COLOR=`[[ $HAVE_IRSSI = '0' ]] && echo "$RED"  || ([[ $IS_IRSSI_STARTED = '1' ]] && echo "$GREEN" || echo "$YELLOW")`

	echo -e "${RT_COLOR}rtorrent${END} ${IRSSI_COLOR}irssi${END} ${VNC_COLOR}vnc${END} - $USER"
}

auto() {
	checkUser

	if [ "$ACC_STATUS" = 'L' ] ; then
		echo "$USER - konto zostało zablokowane"

		if [ $HAVE_RT != '0' ] || [ $HAVE_VNC != '0' ] || [ $HAVE_IRSSI != '0' ] ; then
			if [ "$IS_RT_STARTED" != '0' ] && [ "$IS_VNC_STARTED" != '0' ] && [ "$IS_IRSSI_STARTED" != '0' ] ; then
				stop
			fi
		fi

	else
		start
	fi
}

block() {
	checkUser

	if [ "$ACC_STATUS" = 'L' ] ; then
		echo "$USER - konto jest już zablokowane"
	else
		stop
		passwd -l $USER >/dev/null 2>&1
		echo "$USER - konto zostało zablokowane"
	fi
}

unlock() {
	checkUser

	if [ "$ACC_STATUS" = 'L' ] ; then
		passwd -u $USER
		start
		echo "$USER - konto zostało odblokowane"
	else
		echo "$USER - konto nie jest zablokowane"
	fi
}

delete() {
	checkUser

	## Stop all processes
	stop

	## We need to be sure..
	pkill -u $USER

	## very sure..
	rm -rf /var/run/screen/S-$USER

	deluser --remove-home --remove-all-files $USER > /dev/null 2>&1
	rm -rf $RUTORRENT_PATH/conf/users/$USER
	sed -i "/$USER/g" /usr/local/nginx/rutorrent_passwd
	sed -i '/^$/d' /usr/local/nginx/rutorrent_passwd

	if [ ! -d $ARCHIVE_PATH ] ; then
		mkdir $ARCHIVE_PATH
	fi

	mkdir $ARCHIVE_PATH/$USER
	mv $RUTORRENT_PATH/share/users/$USER $ARCHIVE_PATH/$USER/rutorrent

	cat /usr/local/nginx/logs/access.log | grep $USER | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{0,3}' | sort -u >> $ARCHIVE_PATH/$USER/$USER.ip
	cat /usr/local/nginx/logs/access.log.1 | grep $USER | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{0,3}' | sort -u >> $ARCHIVE_PATH/$USER/$USER.ip
	zcat /usr/local/nginx/logs/*gz | grep $USER | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{0,3}' | sort -u >> $ARCHIVE_PATH/$USER/$USER.ip

	if [ -d $ARCHIVE_PATH/$USER/$USER.ip ] ; then
		cat $ARCHIVE_PATH/$USER/$USER.ip | sort -u > $ARCHIVE_PATH/$USER.ip
	fi

	tar -czvf $ARCHIVE_PATH/$USER.tar.gz -C $ARCHIVE_PATH/$USER > /dev/null 2>&1
	rm -R $ARCHIVE_PATH/$USER

	if [ -d /home/$USER ] ; then
		rm -rf /home/$USER
	fi

	if [ -d /home2/$USER ] ; then
		rm -rf /home2/$USER
	fi

	echo "$USER - konto zostało usunięte"
}



if [ "$USER" = "all" ] ; then
	USERS=`ls -l /home* | grep '^d' | grep -v ftp | grep -v lost+found | awk '{print $9}'`

	if [ $ACTION = "start" ] || [ $ACTION = "stop" ] || [ $ACTION = "restart" ] || [ $ACTION = "status" ] || [ $ACTION = "auto" ] || [ $ACTION = "block" ] || [ $ACTION = "unlock" ] ; then
		for User in $USERS ; do
			$PATH_SCRIPT $ACTION $User
		done
	fi
else
	if [ "$ACTION" = "add" ] ; then
		prepare_create
	elif [ "$ACTION" = "start" ] ; then
		start
	elif [ "$ACTION" = "stop" ] ; then
		stop
	elif [ "$ACTION" = "restart" ] ; then
		restart
	elif [ "$ACTION" = "status" ] ; then
		status
	elif [ "$ACTION" = "auto" ] ; then
		auto
	elif [ "$ACTION" = "block" ] ; then
		block
	elif [ "$ACTION" = "unlock" ] ; then
		unlock
	elif [ "$ACTION" = "delete" ] || [ "$ACTION" = "remove" ] ; then
		delete
	else
		echo "$PATH_SCRIPT [start|stop|restart|status|auto|block|unlock|delete] [nick]"
	fi
fi
