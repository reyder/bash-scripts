#!/bin/bash
# Woox.pl © 2016
# Server statics info updater

# Variables
URL="https://panel.woox.pl/server_stats.php"

# Options for Curl
CURL_HERADER="Content-Type:application/json"
CURL_OPTIONS="--silent -H \"$CURL_HERADER\" --tlsv1.2 -X POST --data"


get_diskspace() {
	read USED AVAILABLE <<< $(df -h -k /home* | awk '{ print $3" "$4 }' | tail -n +2)
}

get_load() {
	LOAD=$(top -bn 1 | awk -v n=$(grep -c ^processor /proc/cpuinfo) 'NR > 7 { s += $9 } END { print int(s / n + .5); }')
}

get_bytes() {
	read DOWNLOAD UPLOAD <<< `awk -v a="$(awk '/eth0: /{print $2,$10}' /proc/net/dev; sleep 1)" '/eth0: /{split(a,b," "); print int(($2-b[1])/1024),int(($10-b[2])/1024)}' /proc/net/dev`
}

# Get all data
get_diskspace
get_load
get_bytes

DATA=$(< <(cat <<EOF
info={
    "load": $LOAD,
    "upload": $UPLOAD,
    "download": $DOWNLOAD,
    "disk_used": $USED,
    "disk_free": $AVAILABLE
}
EOF
))

curl $CURL_OPTIONS "$DATA" $URL
