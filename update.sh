#!/bin/bash
# Woox.pl © 2015

# Those commands are to help sync servers scripts up to date

# enter our repository folder
cd ~/scripts

# fetch changes, git stores them in FETCH_HEAD
git fetch

# check for remote changes in origin repository
UPDATE=`git diff HEAD FETCH_HEAD`

if [ "$UPDATE" != "" ] ; then
	# we don't care about anything, just want to have everything up to date
	git reset --hard FETCH_HEAD
	# .gitignore 3 2 1
	git ls-files --ignored --exclude-standard | sed 's/.*/"&"/' | xargs git rm
	# SetUp privilages
	chmod 740 *
fi
