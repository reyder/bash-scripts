#!/bin/bash
#
# Quota info
# Version 2.1
#
# Copyright (C) 2015  Woox.pl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

QUOTA_PATH="/var/rutorrent"

repquota -a | grep 0 | awk '{ if  ($4 > 0 && $1 != "***") {print $1 ";" sprintf("%.f", $3*1024) ";" sprintf("%.f", $4*1024)}}' > "$QUOTA_PATH/quota.txt"

### Setting privilages
chown nginx:nginx "$QUOTA_PATH/quota.txt"
chmod 700 "$QUOTA_PATH/quota.txt"